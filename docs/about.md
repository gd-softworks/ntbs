# NTBS (Not Too Busy Space)

An open-source platform to organize your projects, ideas and life. Together we create a better ecosystem with many customization options that expand mind and help us to work, study and explore.

## Space concept

**The Space concept** is the fundamental part of our app, it says, "Any work or research can be presented as a connection of divided spaces with single-tasking modules inside them".

You need only 3 components to organize anything:

- Hub

    A sort of homepage where you can manage your spaces and general settings.

- Spaces

    Spaces are collections of modules & data. They are divided according to some characteristic, such as teams, type of your notes, personal & work, etc. 

- Modules

    Modules are the features that you can add, remove or configure inside a space.

## Stack & Architecture 🛠️

- Frontend
    - [React](https://react.dev/)
    - [Tailwind CSS](https://tailwindcss.com/)
    - [PostCSS](https://postcss.org/)
        - [Autoprefixer plugin](https://autoprefixer.github.io/)
    - [Vite](https://vitejs.dev/)
    - [Electron](https://www.electronjs.org/)
    - [React Native](https://reactnative.dev/)
    - State manager (?)
- Backend:
    - [Django](https://www.djangoproject.com/)
    - [PostgreSQL](https://www.postgresql.org/)
    - [Redis](https://redis.io/)
- External (APIs, libs and tools):
    - [Storybook](https://storybook.js.org/) — a frontend workshop for building UI components and pages in isolation
    - [CodeMirror](https://codemirror.net/) — a code editor component (implements markdown input)
    - [remark](https://remark.js.org/) — markdown parser and compiler
        - plugin list coming soon
    - [Prism](https://prismjs.com/) — syntax highlighter
    - [Mermaid](https://mermaid.js.org/) — diagramming and charting tool for Markdown (may be implemented with remark)
    - [MathJax](https://www.mathjax.org/) — display engine for math (may be implemented with remark)
    - [D3.js](https://d3js.org/) — a library for data visualization (needed for the graph view)
    - [Penpot](https://penpot.app/) — The Open-Source design & prototyping platform.

We are building a client-server project that includes: 

- web app (React)
- desktop app (Electron)
- mobile app (React Native)
- some external APIs for plugins, customization and others (will be discussed during the development). 

The architecture is monolithic.

## Features

List of the main features that must be implemented is shown below. Some details or additional feature may be added during the development process.

There are 3 categories of features:

1. Foundation — the fundamental functionality without which the rest cannot be fully implemented.
2. Modules — main plugins (available by default). Each of them have specific functions.
3. Components — different features for better user experience.

### Foundation

#### Hubs

- change name
- add/remove description
- add/remove image (logo)
- add/remove spaces
- manage tags[^tags] for a specific space or globally
- make hub private/public (everyone will be able to find the hub in the ["Explore"](#explore-section) section, just like a public repository)

#### Spaces

- change name
- add/remove description
- add/remove image (logo)
- File browser:
    - add/remove folders
    - add/remove files
    - add/remove modules
    - manage tags
    - make space private/public (to specific people, roles or everyone who uses NTBS)

#### Graph view

- Connections between spaces and the Hub
- Connection between modules in spaces
- Change node color depending on the main tag color
- Change the node size depending on the number of links that refers to this node
- Physics
- Feedback ob user actions
- View settings (change node size, sort by tags, number of links, etc.)
    
#### Manage users:

- add/remove users
- manage user rights to view & edit files, folders and modules
- manage user rights to manage hub & spaces

#### Account management

- Create new account with email, password and nickname
    - Password must follow the standard requirements
    - Each user gets a unique ID (that can be shared with other people). P. S. ID is just a big number that can be copied by clicking a button "Copy user ID"
    - Edit profile
        - change name
        - add/remove avatar
        - add/remove description
- Delete an account (password confirmation required)
- Password recovery
- Sign up/in with third-party accounts (Google, GitHub, etc.)
- 2FA

### Modules

Each module have a name and shows as a single node in a graph. Tags can be assigned to any module and this will colour the node in the appropriate color.

#### Editor module (aka Main module, Zettelkasten module)

- Markdown editor (using CodeMirror and remark)
    - create a new file
    - add tags with #tag syntax
    - add YAML metadata[^YAML]
    - undo/redo changes
    - support of syntax highlighting with Prism
    - diagrams and charts (using Mermaid)
    - maths (using MathJax)
- Links
    - add links to files and modules
    - attach files and preview them (for instance, `![](../attachments/image.jpg)` will show the image in the preview mode)
    - preview links content
    - space links with a specific syntax: `[Something]{SPACE}(/path/to/something)`
- Preview text
    - The preview must be enabled by default for the whole text, except the current typing line (paragraph) — it has to show the original Markdown & YAML syntax.
    - A button to enable/disable preview-only mode
    - A button to enable/disable source view mode — Markdown & YAML syntax for the whole document

#### To-do

- add/delete tasks
- custom states (more than 2 states (to-do/done aka checked/unchecked) can be added)
- view-mods:
    - list
    - kanban
    - sync view-mods[^tags-sync]
- tags management
- assign task to someone

#### Calendar



#### Database



#### Chat



#### Stats



### Components

#### Search



#### Explore section



#### Export files



#### Attach translation 



[^YAML]: The example of YAML metadata:

            ---
            tags: permanent, development
            created: 30-12-2023
            author: Alandor
            ---

[^tags]: Tags have 5 properties:
    - Name
    - Description (optional)
    - Color
    - Visibility (show/hide tags for a specific space)
    - Hierarchy (the first tag is the list is the main one)

[^tags-sync]: To sync list and kanban board view-mods we use tags for tasks. User has an ability to sort columns by tags. For instance, each column is assigned a tag: To-do (to-do), In progress (WIP), Done (done). Both tags and column names can be changed by users.

    Newly added task will be marked as To-do (or any other tag that user choose). Tasks with active checkboxes in the list mode must be placed in Done kanban column by default. 
    
    It's similar with what GitLab & GitHub Projects do.
