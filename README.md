# About Project

NTBS (Not Too Busy Space) is open-source platform to organize your projects, ideas and life. Together we create a better ecosystem with many customization options that expand mind and help us to work, study and explore.
